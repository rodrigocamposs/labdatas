<?php

require_once "../models/Agenda.php";

$data = [];
$agenda = new Agenda();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $acao = filter_input(INPUT_GET, 'acao');
    switch ($acao) {
        case 'calendar':
            $start = filter_input(INPUT_GET, 'start');
            $end = filter_input(INPUT_GET, 'end');
            $agenda = new Agenda();
            $data = $agenda->visualizaAgendamentos(compact('start', 'end'));
            break;
        case 'buscaAgenda':
            $id_agenda = filter_input(INPUT_GET, 'id_agenda');
            $data = $agenda->buscaAgendamento($id_agenda);
            break;
        case 'pesquisa':
            $termo = filter_input(INPUT_GET, 'q');
            $data = $agenda->pesquisaAgendamentos($termo);
            break;
        default:
            break;
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $acao = filter_input(INPUT_POST, 'acao');
    $dados = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    switch ($acao) {
        case 'cadastro':
            $data = $agenda->insert($dados);
            break;
        case 'edicao':
            $data = $agenda->editaAgendamento($dados);
            break;
        case 'mover':
            $data = $agenda->atualizaAgendamento($dados);
            break;
        case 'excluir':
            $data = $agenda->excluiAgendamento($dados);
            break;
        default :
            break;
    }
    header('Content-Type: application/json');
    echo json_encode($data);
    exit;
}
