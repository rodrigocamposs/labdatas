<?php    
    
    ini_set('display_errors', true);
    error_reporting(E_ALL);

    require_once "../models/Pacientes.php";

    if ($_POST['acao'] == 'cadastro') :

        # Sanitiza o POST - Limpa o POST
        
        $dados = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $paciente = new Pacientes();

        $paciente->insert($dados);

        header("location: ../views/pacientes.php");

    endif;

    if ($_POST['acao'] == 'edicao') :

        # Sanitiza o POST - Limpa o POST
        
        $dados = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $paciente = new Pacientes();

        $paciente->editaPaciente($dados);

        header("location: ../views/pacientes.php");
    endif;    

    if ($_POST['acao'] == 'buscaPaciente') :

        # Sanitiza o POST - Limpa o POST
        
        $dados = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $id = $dados['id'];

        $paciente = new Pacientes();

        $row = $paciente->buscaPaciente($id);

        echo json_encode($row);

        exit();

    endif;
    
    if ($_POST['acao'] == 'financeiro') :

        # Sanitiza o POST - Limpa o POST
        
        $dados = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $paciente = new Pacientes();

        $paciente->insereFundos($dados);

        header("location: ../views/financeiro.php");
        
    endif;
