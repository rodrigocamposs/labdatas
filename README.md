# LabDatas
###Sistema Open Source para laboratórios públicos

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lacus diam quisque tincidunt gravida a gravida, massa a adipiscing class ut tellus, ligula quis quisque libero potenti. morbi inceptos lacinia quisque duis vitae ullamcorper suspendisse tempus nulla, habitant urna scelerisque cursus venenatis pellentesque elit ligula ac, luctus urna velit fusce feugiat aenean duis elementum. turpis aenean nunc bibendum mi quam cubilia adipiscing lacinia, sem molestie placerat duis fames ornare cras maecenas, interdum habitant imperdiet cursus blandit tempor velit. gravida nam vestibulum sem curabitur neque vitae varius, gravida rutrum iaculis potenti consectetur vestibulum ut suspendisse, rutrum cursus cubilia sagittis placerat adipiscing. 

## Preparando o ambiente de desenvolvimento
```bash
git clone git@bitbucket.org:rodrigocamposs/labdatas.git
```

Atualize a versão do PHP para **php7.3**

Caso necessário use o repositório 

```bash
wget -O /etc/apt/trusted.gpg.d/ondrej-debian-php-jessie.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ jessie main" > /etc/apt/sources.list.d/ondrej-debian-php-jessie.list
apt-get install -y apt-transport-https lsb-release ca-certificates
apt-get update
apt-get install -y php-7.3 php-7.3-mysql
```

Resolvendo dependências com **Bower**
```bash
apt-get install npm
npm install -g bower
ln -s /usr/bin/node{js,}
```

## Créditos
Libero bibendum vel at litora ut sapien, auctor per etiam sagittis rhoncus litora ultricies, curabitur lobortis dolor diam quisque. phasellus nec volutpat ipsum ligula laoreet congue nulla suspendisse elit, curae feugiat faucibus condimentum porttitor adipiscing netus accumsan integer, arcu dui ultricies potenti pulvinar duis eros dictum.

##Contato
Telegram (grupo): [LabDatas](https://t.me/joinchat/HUdxVg8gUNuyO2ElsTfkew) 