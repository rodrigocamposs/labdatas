<?php

class Layout {

    const BG_AQUA = 'bg-aqua';
    const BG_BLUE = 'bg-blue';
    const BG_FUCHSIA = 'bg-fuchsia';
    const BG_YELLOW = 'bg-yellow';
    const BG_TEAL = 'bg-teal';
    const BG_PURPLE = 'bg-purple';
    const BG_RED = 'bg-red';
    const BG_GRAY = 'bg-gray';
    const BG_GREEN = 'bg-green';
    const BG_LIGHT_BLUE = 'bg-light-blue';
    const BG_LIME = 'bg-lime';
    const BG_MAROON = 'bg-maroon';
    const BG_NAVY = 'bg-navy';
    const BG_OLIVE = 'bg-olive';
    const BG_ORANGE = 'bg-orange';
    const BG_BLACK = 'bg-black';
    const BG_AQUA_ACTIVE = 'bg-aqua-active';
    const BG_BLUE_ACTIVE = 'bg-blue-active';
    const BG_FUCHSIA_ACTIVE = 'bg-fuchsia-active';
    const BG_YELLOW_ACTIVE = 'bg-yellow-active';
    const BG_TEAL_ACTIVE = 'bg-teal-active';
    const BG_PURPLE_ACTIVE = 'bg-purple-active';
    const BG_RED_ACTIVE = 'bg-red-active';
    const BG_GRAY_ACTIVE = 'bg-gray-active';
    const BG_GREEN_ACTIVE = 'bg-green-active';
    const BG_LIGHT_BLUE_ACTIVE = 'bg-light-blue-active';
    const BG_LIME_ACTIVE = 'bg-lime-active';
    const BG_MAROON_ACTIVE = 'bg-maroon-active';
    const BG_NAVY_ACTIVE = 'bg-navy-active';
    const BG_OLIVE_ACTIVE = 'bg-olive-active';
    const BG_ORANGE_ACTIVE = 'bg-orange-active';
    const BG_BLACK_ACTIVE = 'bg-black-active';
    const BG_AQUA_GRADIENT = 'bg-aqua-gradient';
    const BG_BLUE_GRADIENT = 'bg-blue-gradient';
    const BG_YELLOW_GRADIENT = 'bg-yellow-gradient';
    const BG_TEAL_GRADIENT = 'bg-teal-gradient';
    const BG_PURPLE_GRADIENT = 'bg-purple-gradient';
    const BG_RED_GRADIENT = 'bg-red-gradient';
    const BG_GREEN_GRADIENT = 'bg-green-gradient';
    const BG_LIGHT_BLUE_GRADIENT = 'bg-light-blue-gradient';

    protected $_colors = [
        self::BG_AQUA => '#00c0ef',
        self::BG_BLUE => '#0073b7',
        self::BG_FUCHSIA => '#f012be',
        self::BG_YELLOW => '#f39c12',
        self::BG_TEAL => '#39cccc',
        self::BG_PURPLE => '#605ca8',
        self::BG_RED => '#dd4b39',
        self::BG_GRAY => '#d2d6de',
        self::BG_GREEN => '#00a65a',
        self::BG_LIGHT_BLUE => '#3c8dbc',
        self::BG_LIME => '#01ff70',
        self::BG_MAROON => '#d81b60',
        self::BG_NAVY => '#001f3f',
        self::BG_OLIVE => '#3d9970',
        self::BG_ORANGE => '#ff851b',
        self::BG_BLACK => '#111111',
        self::BG_AQUA_ACTIVE => '#00a7d0',
        self::BG_BLUE_ACTIVE => '#005384',
        self::BG_FUCHSIA_ACTIVE => '#db0ead',
        self::BG_YELLOW_ACTIVE => '#db8b0b',
        self::BG_TEAL_ACTIVE => '#30bbbb',
        self::BG_PURPLE_ACTIVE => '#555299',
        self::BG_RED_ACTIVE => '#d33724',
        self::BG_GRAY_ACTIVE => '#b5bbc8',
        self::BG_GREEN_ACTIVE => '#008d4c',
        self::BG_LIGHT_BLUE_ACTIVE => '#357ca5',
        self::BG_LIME_ACTIVE => '#00e765',
        self::BG_MAROON_ACTIVE => '#ca195a',
        self::BG_NAVY_ACTIVE => '#001a35',
        self::BG_OLIVE_ACTIVE => '#368763',
        self::BG_ORANGE_ACTIVE => '#ff7701',
        self::BG_BLACK_ACTIVE => '#000000',
        self::BG_AQUA_GRADIENT => '#00c0ef',
        self::BG_BLUE_GRADIENT => '#0073b7',
        self::BG_YELLOW_GRADIENT => '#f39c12',
        self::BG_TEAL_GRADIENT => '#39cccc',
        self::BG_PURPLE_GRADIENT => '#605ca8',
        self::BG_RED_GRADIENT => '#dd4b39',
        self::BG_GREEN_GRADIENT => '#00a65a',
        self::BG_LIGHT_BLUE_GRADIENT => '#3c8dbc',
    ];

    public function colors() {
        return $this->_colors;
    }

}
