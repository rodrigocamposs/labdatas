<?php
    

    class template
    {
        private $title;
        private $active_index;
        private $active_estatisticas;
        private $active_pacientes;
        private $active_agenda;
        private $active_exames;


        public function inibody()
        {
            ?><!DOCTYPE HTML>

            <HTML lang="pt-br">
            <meta charset="UTF-8"/>
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">
                <title><?= $this->getTitle() ?></title>
                <!-- Bootstrap Core CSS -->
                <link href="/public/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

                <!-- Custom CSS -->
                <link href="/public/css/sb-admin.css" rel="stylesheet">
                <link href="/public/css/css.css" rel="stylesheet">

                <!-- Custom Fonts -->
                <link href="/public/plugins/components-font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

                <!-- Fullcalendar -->
                <link href='/public/plugins/fullcalendar/dist/fullcalendar.min.css' rel='stylesheet' />
                <link href='/public/plugins/fullcalendar/dist/fullcalendar.print.min.css' rel='stylesheet' media='print' />
                
                <!-- inputmask -->
                <link href='/public/plugins/inputmask/css/inputmask.css' rel='stylesheet' />
                
                <!-- Select2 -->
                <link href='/public/plugins/select2/dist/css/select2.min.css' rel='stylesheet' />
                
                <!-- Datetimepicker -->
                <link href='/public/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
                
                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
            <?php
        }

        public function menu()
        {
            ?>
            <div id="wrapper">

                <!-- Navigation -->
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">LAB DATAS</a>
                    </div>
                    <!-- Top Menu Items -->
                    <ul class="nav navbar-right top-nav">


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Jose Maria <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Mensagens</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracoes</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li class="<?= $this->getActiveIndex()?>">
                                <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Painel Inicial</a>
                            </li>
                            <li class="<?= $this->getActiveEstatisticas()?>">
                                <a href="financeiro.php"><i class="fa fa-fw fa-bar-chart-o"></i> Financeiro</a>
                            </li>
                            <li class="<?= $this->getActivePacientes()?>">
                                <a href="pacientes.php"><i class="fa fa-fw fa-users"></i> Pacientes</a>
                            </li>
                            <li class="<?= $this->getActiveAgenda()?>">
                                <a href="agenda.php"><i class="fa fa-fw fa-table"></i> Agenda</a>
                            </li>
                            <li class="<?= $this->getActiveExames()?>">
                                <a href="exames.php"><i class="fa fa-fw fa-flask"></i> Exames</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>



            <!-- /#wrapper -->


            <?php
        }

        public function fimbody()
        {
            ?>
            </div>
            </body>
            </HTML>
        <?php
        }


        public function setTitle($title)
        {
            $this->title = $title;
        }

        public function getTitle()
        {
            return $this->title;
        }




        /**
         * @return mixed
         */
        public function getActiveIndex()
        {
            return $this->active_index;
        }

        /**
         * @param mixed $active_index
         */
        public function setActiveIndex($active_index)
        {
            $this->active_index = $active_index;
        }

        /**
         * @return mixed
         */
        public function getActiveEstatisticas()
        {
            return $this->active_estatisticas;
        }

        /**
         * @param mixed $active_estatisticas
         */
        public function setActiveEstatisticas($active_estatisticas)
        {
            $this->active_estatisticas = $active_estatisticas;
        }

        /**
         * @return mixed
         */
        public function getActivePacientes()
        {
            return $this->active_pacientes;
        }

        /**
         * @param mixed $active_pacientes
         */
        public function setActivePacientes($active_pacientes)
        {
            $this->active_pacientes = $active_pacientes;
        }

        /**
         * @return mixed
         */
        public function getActiveAgenda()
        {
            return $this->active_agenda;
        }

        /**
         * @param mixed $active_agenda
         */
        public function setActiveAgenda($active_agenda)
        {
            $this->active_agenda = $active_agenda;
        }

        /**
         * @return mixed
         */
        public function getActiveExames()
        {
            return $this->active_exames;
        }

        /**
         * @param mixed $active_exames
         */
        public function setActiveExames($active_exames)
        {
            $this->active_exames = $active_exames;
        }
    }
