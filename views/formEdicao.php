<?php
  include_once "template/template.php";
  $template = new template();
  $template->setTitle("LAB Datas - Gestao de laboratorios municipais");
  $template->setActivePacientes("active");
  $template->inibody();
  $template->menu();

  require_once "../models/Pacientes.php";
  $paciente = new Pacientes();
  $id = $_POST['idPaciente'];
  $pacientes = $paciente->buscaPaciente($id);
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edição de Pacientes - <?php echo $pacientes['nome'] ?>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Editar Paciente
                            </li>
                        </ol>
                    </div>
                </div>


                <div>
                    <div class="col-lg-12">
                      <form class="" action="../controllers/Func_Controller.php" method="post">
                          <input type="hidden" name="acao" value="edicao">

                          <input type="hidden" name="id_paciente" value="<?php echo $pacientes['id_paciente'] ?>">

                      <div class="form-group">
                          <label>Nome Completo do Paciente</label>
                          <input class="form-control" name="nome" placeholder="Digite o nome do paciente" value="<?php echo $pacientes['nome'] ?>">
                      </div>
                      <div class="form-group">
                          <label>Sexo</label>                         
                          <input type="radio" name="sexo" value="1" <?php echo $pacientes['sexo'] == 1 ? 'checked' : '' ?> > Masculino
                          <input type="radio" name="sexo" value="0" <?php echo $pacientes['sexo'] == 0 ? 'checked' : '' ?> > Feminino
                      </div>
                      <div class="form-group">
                          <label>Nome Completo da Mãe Paciente</label>
                          <input class="form-control" name="nome_mae" placeholder="Digite o nome completo da Mãe do paciente" value="<?php echo $pacientes['nome_mae'] ?>">
                      </div>
                      <div class="form-group col-md-4">
                          <label>RG do paciente</label>
                          <input class="form-control" name="rg_paciente" placeholder="Digite o RG do Paciente" value="<?php echo $pacientes['rg'] ?>">
                      </div>
                      <div class="form-group col-md-4">
                          <label>Data de Nascimento</label>
                          <input class="form-control" type="date" name="data_nasc" placeholder="Data de nascimento do paciente" value="<?php echo $pacientes['data_nasc'] ?>">
                      </div>                      
                      <div class="form-group col-md-4">
                          <label>Número do cartão SUS</label>
                          <input class="form-control" name="num_sus" placeholder="Número do cartão SUS do Paciente" value="<?php echo $pacientes['num_sus'] ?>">
                      </div>
                      <div class="form-group">
                          <label>Telefone</label>
                          <input class="form-control" name="contato" placeholder="Telefone do Paciente" value="<?php echo $pacientes['contato'] ?>">
                      </div>
                      <div class="form-group">
                          <label>Endereço</label>
                          <input class="form-control" name="endereco" placeholder="Endereço do Paciente" value="<?php echo $pacientes['endereco'] ?>">
                      </div>
                      <div class="">
                        <button type="submit" class="btn btn-success btn-lg">Salvar</button>
                        <button type="submit" href="pacientes.php" class="btn btn-danger btn-md">Cancelar</button>
                      </div>
                    </div>
                    </form>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>


<?php
$template->fimbody();
?>
