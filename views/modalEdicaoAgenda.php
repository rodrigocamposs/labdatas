<div id="editar-agenda-modal" class="modal fade" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">
                    <i class="glyphicon glyphicon-check text-primary"></i>
                    <strong class="text-primary"> AGENDAMENTO - EDITAR/EXCLUIR </strong>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label class="control-label required">Paciente</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                                <select class="form-control select2" name="id_paciente" id="id_paciente" style="width: 100%;">
                                    <?php foreach ($pacientes as $id => $paciente) : ?>
                                        <option value="<?= $id ?>"><?= $paciente ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label required">Horário</label>
                            <div class="input-group">
                                <input name="data_hora_inicial" id="data_hora_inicial" class="form-control datetime" placeholder="Data Inicial" type="text">                            
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                <input name="data_hora_final" id="data_hora_final" class="form-control datetime" placeholder="Data Final" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-lg-4">
                            <label class="control-label required">Exame</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                                <select class="form-control select2" name="id_tipoexame" id="id_tipoexame" style="width: 100%;">
                                    <?php foreach ($exames as $id => $exame) : ?>
                                        <option value="<?= $id ?>"><?= $exame ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label required">Médico</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                                <select class="form-control select2" name="id_medico" id="id_medico" style="width: 100%;">
                                    <?php foreach ($medicos as $id => $medico) : ?>
                                        <option value="<?= $id ?>"><?= $medico ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label">Local</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                                <select class="form-control select2" name="id_local" id="id_local" style="width: 100%;">
                                    <?php foreach ($locais as $id => $local) : ?>
                                        <option value="<?= $id ?>"><?= $local ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="id_agenda" name="id_agenda">
                <input type="hidden" name="acao" value="edicao">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" id="excluir-agenda">Excluir</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" id="editar-agenda">Salvar Alterações</button>
            </div>
        </div>
    </div>
</div>