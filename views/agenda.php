<?php
include_once 'template/template.php';
require '../models/Pacientes.php';
require '../models/Medicos.php';
require '../models/Exames.php';
require '../models/Local.php';
$template = new template();
$template->setTitle("LAB Datas - Agendamento");
$template->setActiveAgenda("active");
$template->inibody();
$template->menu();
$pacientes = Pacientes::getPacientes();
$exames = Exames::getTipoExames();
$medicos = Medicos::getMedicos();
$locais = Local::getLocais();
?>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-4">
                <h3 class="text-center text-primary page-header">
                    <strong>&nbsp;</strong>
                </h3>
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Agendamentos
                        </div>
                        <div class="panel-body">
                            <div class="input-group">
                                <input id="pesquisa-texto" name="q" class="form-control" placeholder="Digite nome, data ou exame para pesquisar agenda" type="text">
                                <div class="input-group-btn">
                                    <button id="pesquisar-agenda" type="button" class="btn btn-default btn-flat" title="Pesquisar"><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
                                    <button id="limpar-pesquisa" type="button" class="btn btn-danger btn-flat" title="Limpar Pesquisa"><span class="fa fa-times"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div id="novo-agendamento">
                                <div class="form-group">
                                    <label class="control-label required">Paciente</label>
                                    <select name="id_paciente" class="form-control select2" placeholder="Selecione Paciente" style="width: 100%;">
                                        <option value=""></option>
                                        <?php foreach ($pacientes as $id => $paciente) : ?>
                                            <option value="<?= $id ?>"><?= $paciente ?></option>>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label required">Exame</label>
                                    <select name="id_tipoexame" class="form-control select2" placeholder="Selecione Exame" style="width: 100%;">
                                        <option value=""></option>
                                        <?php foreach ($exames as $id => $exame) : ?>
                                            <option value="<?= $id ?>"><?= $exame ?></option>>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label required">Médico</label>
                                    <select name="id_medico" class="form-control select2" placeholder="Selecione Médico" style="width: 100%;">
                                        <option value=""></option>
                                        <?php foreach ($medicos as $id => $medico) : ?>
                                            <option value="<?= $id ?>"><?= $medico ?></option>>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Local</label>
                                    <select name="id_local" class="form-control select2" placeholder="Selecione Local" style="width: 100%;">
                                        <option value=""></option>
                                        <?php foreach ($locais as $id => $local) : ?>
                                            <option value="<?= $id ?>"><?= $local ?></option>>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <label class="control-label required">Horário</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input name="data_hora_inicial" id="dt_inicial" class="form-control datetime" placeholder="Data Inicial" type="text">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <input name="data_hora_final" id="dt_final" class="form-control datetime" placeholder="Data Final" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button id="cadastrar-agenda" type="button" class="btn btn-primary btn-flat">Cadastrar</button>
                                </div>
                                <input name="acao" type="hidden" value="cadastro">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <h3 class="text-center text-primary page-header"> <strong>Agenda</strong> </h3>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<?php include('modalEdicaoAgenda.php') ?>
<!-- jQuery -->
<script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<!-- fullcalendar -->
<script src='/public/plugins/moment/min/moment.min.js' type="text/javascript"></script>
<script src='/public/plugins/fullcalendar/dist/fullcalendar.min.js' type="text/javascript"></script>
<script src='/public/plugins/fullcalendar/dist/locale/pt-br.js' type="text/javascript"></script>
<!-- inputmask -->
<script src="/public/plugins/inputmask/dist/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<!-- select2 -->
<script src="/public/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script src="/public/plugins/select2/dist/js/i18n/pt-BR.js" type="text/javascript"></script>
<!-- Datetimepicker -->
<script src="/public/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- -->
<script src="agenda.js" type="text/javascript"></script>
<?php
$template->fimbody();
