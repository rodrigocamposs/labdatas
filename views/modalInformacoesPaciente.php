  <!-- INICIO MODAL EDIÇÃO -->
  <div id="myModal1" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h4 class="modal-title"><i class="glyphicon glyphicon-check text-primary"></i> <strong class="text-primary"> PACIENTE - DETALHES </strong> </h4>
        </div>

        <div class="modal-body">
          <div id="modal-loader" style="display: none; text-align: center;">
            <img src="/public/img/loading.gif" width="40%">
          </div>
          <div id="detalhes">

            <div class="row" id="tabela1">
            
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border row">
                    <span class="label label-primary"></span>

                      <div class="box-body">

                        <div class="row col-md-12">

                          <div class="form-group col-md-12">
                            <label>Nome</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                              <input class="form-control" name="nome" id="nome" type="text" value="">
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <label>RG</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                              <input class="form-control" name="rg" id="rg" type="text" value="">
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <label>Numero do SUS</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                              <input class="form-control" name="num_sus" id="num_sus" type="text" value="">
                            </div>
                          </div>                          

                          <div class="form-group col-md-6">
                            <label>Contato</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                              <input class="form-control" name="contato" id="contato" type="text" value="">
                            </div>
                          </div>
    
                          <div class="form-group col-md-6">
                            <label>Endereço</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                              <input class="form-control" name="endereco" id="endereco" type="text" value="">
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <label>Data Nascimento</label>
                            <div class="input-group">
                              <div class="input-group-addon"><i class="fa fa-list text-primary"></i></div>
                              <input class="form-control" name="dt_nasc" id="dt_nasc" type="text" value="">
                            </div>
                          </div>                          

                        </div>

                        <div class="box-footer">
                          <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" aria-hidden="true">Fechar</button>
                        </div>

                      </div>

                  </div>                  
                </div>

              </div>

            </div>

          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- FINAL MODAL EDIÇÃO -->