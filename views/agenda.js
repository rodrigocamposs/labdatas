$(document).ready(function () {
    var filter = ['all'];
    function actionAgenda(data) {
        $.ajax({
            url: '../controllers/Agenda_Controller.php',
            data: data,
            async: true,
            method: "post",
            dataType: "json"
        }).done(function (data, status, xhr) {
            $('#calendar').fullCalendar('refetchEvents');
            $('#novo-agendamento :input:not([name="acao"])').val('').trigger('change');
        }).fail(function (xhr, status, error) {
            console.log(xhr);
        });
    }
    function cadastrarAgenda() {
        var data = $('#novo-agendamento :input').serialize();
        actionAgenda(data);
    }
    function atualizaAgendamento(event) {
        var data = {
            acao: 'mover',
            id_agenda: event.id,
            data_hora_inicial: event.start.format('DD/MM/YYYY HH:mm'),
            data_hora_final: event.end.format('DD/MM/YYYY HH:mm')
        };
        actionAgenda(data);
    }
    function editaAgendamento() {
        var data = $('#editar-agenda-modal :input').serialize();
        actionAgenda(data);
    }
    function excluiAgendamento() {
        var data = {
            acao: 'excluir',
            id_agenda: $('#id_agenda').val()
        };
        actionAgenda(data);
    }
    $('#calendar').fullCalendar({
        lang: 'pt-br',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,list'
        },
        defaultDate: new Date(),
        defaultView: 'month',
        navLinks: true,
        editable: true,
        minTime: '08:00:00',
        maxTime: '18:10:00',
        slotDuration: '00:15:00',
        eventLimit: true,
        eventLimitText: 'Exames',
        views: {
            agenda: {
                eventLimit: 3,
            }
        },
        events: {
            url: '../controllers/Agenda_Controller.php',
            data: {
                acao: 'calendar'
            }
        },
        eventRender: function (event, element, view) {
            var content = '<span class="glyphicon glyphicon-time"></span> '
                    + event.start.format('HH:mm')
                    + ' - '
                    + event.end.format('HH:mm')
                    + event.description;
            element.attr({
                "data-toggle": "popover",
                "data-placement": "left",
                "title": event.title,
                "data-content": content,
                "data-html": "true",
                "data-trigger": "focus",
                "data-container": "body"
            });
            if ('description' in event) {
                element.find('.fc-title').append('<small>' + event.description + '</small>');
            }
            element.bind('dblclick', function () {
                $('#id_agenda').val(event.id);
                $('#editar-agenda-modal').modal('show');
            });
            if (filter.indexOf('all') < 0) {
                return filter.indexOf(event.id) >= 0;
            }
        },
        eventResize: function (event) {
            atualizaAgendamento(event);
        },
        eventDrop: function (event, delta, revertFunc) {
            atualizaAgendamento(event);
        },
        eventMouseout: function (event, js, view) {
            $(this).popover('hide');
        },
        eventMouseover: function (calEvent, jsEvent, view) {
            $("#contextual-menu").fadeOut(200);
            $(this).popover('show');
        },
        dayClick: function (date, jsEvent, view) {
            switch (view.name) {
                case 'month':
                    $('#calendar').fullCalendar('gotoDate', date);
                    $('#calendar').fullCalendar('changeView', 'agendaWeek');
                    break;
                case 'agendaWeek':
                    $('#dt_inicial').val(date.format('DD/MM/YYYY HH:mm'));
                    $('#calendar').fullCalendar('gotoDate', date);
                    $('#calendar').fullCalendar('changeView', 'agendaDay');
                    break;
                case 'agendaDay':
                    $('#dt_inicial').val(date.format('DD/MM/YYYY HH:mm'));
                    break;
                default:
                    console.log(date.format('DD/MM/YYYY HH:mm'));
                    break;
            }
        }
    });
    $(".select2").select2({
        placeholder: "Selecione opção",
        allowClear: true,
        language: "pt-BR"
    });
    $('input.datetime').inputmask("99/99/9999 99:99");
    $('input.datetime').datetimepicker({
        locale: 'pt-br'
    });
    $('#cadastrar-agenda').on('click', function () {
        cadastrarAgenda();
    });
    $('#editar-agenda').on('click', function () {
        editaAgendamento();
    });
    $('#excluir-agenda').on('click', function () {
        excluiAgendamento();
    });
    $('#pesquisar-agenda').on('click', function () {
        var data = $('#pesquisa-texto').serialize() + '&acao=pesquisa';
        $.ajax({
            url: '../controllers/Agenda_Controller.php',
            data: data,
            async: true,
            method: "get",
            dataType: "json"
        }).done(function (data, status, xhr) {
            $('#calendar').fullCalendar('gotoDate', new Date());
            $('#calendar').fullCalendar('changeView', 'listYear');
            filter = data.id_agenda.split(',');
            $('#calendar').fullCalendar('rerenderEvents');
        }).fail(function (xhr, status, error) {
            console.log(xhr);
        });
    });
    $('#limpar-pesquisa').on('click', function () {
        $('#pesquisa-texto').val('');
        filter = ['all'];
        $('#calendar').fullCalendar('gotoDate', new Date());
        $('#calendar').fullCalendar('changeView', 'month');
        $('#calendar').fullCalendar('rerenderEvents');
    });
    $('#editar-agenda-modal').on('show.bs.modal', function () {
        var data = $('#id_agenda').serialize() + '&acao=buscaAgenda';
        $.ajax({
            url: '../controllers/Agenda_Controller.php',
            data: data,
            async: true,
            method: "get",
            dataType: "json"
        }).done(function (data, status, xhr) {
            $('#id_paciente').val(data.id_paciente).trigger('change');
            $('#id_tipoexame').val(data.id_tipoexame).trigger('change');
            $('#id_medico').val(data.id_medico).trigger('change');
            $('#id_local').val(data.id_local).trigger('change');
            var dt = moment(data.data_hora_inicial).format('DD/MM/YYYY HH:mm');
            $('#data_hora_inicial').val(dt);
            dt = moment(data.data_hora_final).format('DD/MM/YYYY HH:mm');
            $('#data_hora_final').val(dt);
        }).fail(function (xhr, status, error) {
            console.log(xhr);
        });
    });
});