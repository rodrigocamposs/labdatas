<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="rodrigo" >

    <title>LAB Datas - Gestao de laboratorios municipais</title>

    <!-- Bootstrap Core CSS -->
    <link href="/public/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/public/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/public/plugins/components-font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">LAB DATAS</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Jose Maria <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Mensagens</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracoes</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Painel Inicial</a>
                    </li>
                    <li>
                        <a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Financeiro</a>
                    </li>
                    <li>
                        <a href="pacientes.php"><i class="fa fa-fw fa-users"></i> Pacientes</a>
                    </li>
                    <li>
                        <a href="agenda.php"><i class="fa fa-fw fa-table"></i> Agenda</a>
                    </li>
                    <li>
                        <a href="exames.php"><i class="fa fa-fw fa-flask"></i> Exames</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>



        <div id="page-wrapper">

            <div class="container-fluid">
          			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Relatório Financeiro
                        </h1>

                    </div>
                </div>
                
                <div class="row">
                
                <form class="form-inline" action="" method="POST">
                    <div class="form-group">
                      <label >Data</label>
                      <input type="date" class="form-control" id="data_fundo" ">
                    </div>
                    <div class="form-group">
                      <label >Valor</label>
                      <input type="number" class="form-control" id="valor_fundo" ">
                    </div>
                    <button type="submit" class="btn btn-success">Adicionar fundo</button>
                  </form>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-hover">
                                      <thead>
                                          <tr>
                                              <th>Data</th>
                                              <th>Entrada</th>
                                              <th>Saída</th>
                                              
                                            
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>                                         
                                              <td>Julho/2018</td>
                                              <td>20000</td>
                                              <td>565</td>
                                              
                                          </tr>
                                          
                                            <tr>                                         
                                              <td>Agosto/2018</td>
                                              <td>20200</td>
                                              <td>300</td>
                                              
                                          </tr>
                                          


                              </tbody>
                              </table>
                          </div>
                      </div>

                </div>



                </div>
              </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

</body>

</html>
