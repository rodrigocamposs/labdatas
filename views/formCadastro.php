<?php
include_once "template/template.php";
$template = new template();
$template->setTitle("LAB Datas - Gestao de laboratorios municipais");
$template->setActivePacientes("active");
$template->inibody();
$template->menu();
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Cadastro de Pacientes
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Cadastrar novo Paciente
                            </li>
                        </ol>
                    </div>
                </div>


                <div>
                    <div class="col-lg-12">
                      <form class="" action="../controllers/Func_Controller.php" method="post">
                          <input type="hidden" name="acao" value="cadastro">

                      <div class="form-group">
                          <label>Nome Completo do Paciente</label>
                          <input class="form-control" name="nome_paciente" placeholder="Digite o nome do paciente">
                      </div>
                      <div class="form-group">
                          <label>Sexo</label>                         
                          <input type="radio" name="sexo" value="1"> Masculino
                          <input type="radio" name="sexo" value="0"> Feminino<br>
                      </div>
                      <div class="form-group">
                          <label>Nome Completo da Mãe Paciente</label>
                          <input class="form-control" name="nome_mae" placeholder="Digite o nome completo da Mãe do paciente">
                      </div>
                      <div class="form-group">
                          <label>RG do paciente</label>
                          <input class="form-control" name="rg_paciente" placeholder="Digite o RG do Paciente">
                      </div>

                      <div class="form-group col-md-4">
                          <label>Data de Nascimento</label>
                          <input class="form-control" type="date" name="data_nasc" placeholder="Data de nascimento do paciente">
                      </div>
                      <div class="form-group col-md-4">
                          <label>Número do cartão SUS</label>
                          <input class="form-control" name="num_sus" placeholder="Número do cartão SUS do Paciente">
                      </div>
                      <div class="form-group col-md-4">
                          <label>Telefone</label>
                          <input class="form-control" name="telefone" placeholder="Telefone do Paciente">
                      </div>
                      <div class="form-group">
                          <label>Endereço</label>
                          <input class="form-control" name="endereco" placeholder="Endereço do Paciente">
                      </div>
                      <div class="">
                        <button type="submit" class="btn btn-success btn-lg">Salvar</button>
                        <button type="button" href="pacientes.php" class="btn btn-danger btn-md">Cancelar</button>
                      </div>
                    </div>
                    </form>
                </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

<?php
$template->fimbody();
?>
