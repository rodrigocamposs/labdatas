<?php
include_once "template/template.php";
$template = new template();
$template->setTitle("LAB Datas - Pacientes");
$template->setActiveExames("active");
$template->inibody();
$template->menu();
?>




<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Banco de Exames
                        </h1>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-hover">
                                      <thead>
                                          <tr>
                                              <th>Numero da visita</th>
                                              <th>Nome</th>
                                              <th>Tipo de Exame</th>
                                              <th>Data</th>
                                              <th>Status</th>
                                              <th>Ação</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td>01</td>
                                              <td>Rodrigo souza campos</td>
                                              <td>Hemograma</td>
                                              <td>Segunda-Feira</td>
                                              <td><span class="label label-success">Finalizado</span></td>
                                              <td>
                                                <button type="button" class="btn btn-warning btn-xs">Inserir Resultados</button>
                                                <button type="button" class="btn btn-primary btn-xs">Imprimir Laudo</button>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>01</td>
                                              <td>Rodrigo souza campos</td>
                                              <td>Urinálise</td>
                                              <td>Segunda-Feira</td>
                                              <td><span class="label label-warning">Aguardando Resultados</span></td>
                                              <td>
                                                <button type="button" class="btn btn-warning btn-xs">Inserir Resultados</button>
                                                <button type="button" class="btn btn-primary btn-xs">Imprimir Laudo</button>
                                              </td>
                                          </tr>


                              </tbody>
                              </table>
                          </div>
                      </div>

                </div>



                </div>
              </div>


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

<?php
$template->fimbody();
?>