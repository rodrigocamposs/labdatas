<?php
include_once "template/template.php";
$template = new template();
$template->setTitle("LAB Datas - Gestao de laboratorios municipais");
$template->setActiveIndex("active");
$template->inibody();
$template->menu();
?>

            <!-- jQuery -->
    <script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script src="../js/plugins/morris/morris-data.js"></script>

<?php
$template->fimbody();
?>