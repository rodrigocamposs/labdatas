
$(document).ready(function () {

  /** Captura o evento de clique no botao #id */
  $(document).on('click', '#id', function (e) {

    /** Previno o evento default do navegador. Para evitar comportamento indesejado por parte do navegador. */
    e.preventDefault();

    /** Captura o id a ser localizado */
    let id = $(this).data('id');

    /** Faço um ajax para o arquivo que faz a consulta no banco e devolve um json */
    $.ajax({
        url: '../controllers/Func_Controller.php',
        type: 'POST',
        data: 'id=' + id + '&acao=' + 'buscaPaciente',     // Dados a serem enviados no POST
        dataType: 'json'
      })

      /** Caso tenha sucesso na consulta */
      .done(function (data) {

        /** Faço a atribuição dos valores no modal. */
        $('#nome').val(data.nome);
        $('#rg').val(data.rg);
        $('#num_sus').val(data.num_sus);
        $('#contato').val(data.contato);
        $('#endereco').val(data.endereco);
        $('#dt_nasc').val(data.data_nasc);

      })

      /** Caso alguma coisa dê errado */
      .fail(function () {
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Dados não disponíveis. Por favor, tente mais tarde...');
      }); 
  });

});