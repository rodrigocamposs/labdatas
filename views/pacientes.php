<?php
    include_once "template/template.php";
    require_once "../models/Pacientes.php";
    $paciente = new Pacientes();
    $template = new template();
    $template->setTitle("LAB Datas - Pacientes");
    $template->setActivePacientes("active");
    $template->inibody();
    $template->menu();
    $pacientes = $paciente->visualizaPacientes();
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <a href="formCadastro.php"> <button type="button" class="btn btn-primary btn-lg">Cadastrar Paciente  </button></a>
                        </h1>
                            <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Pacientes
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <h2>Pacientes Cadastrados</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Sexo</th>
                                        <th>Número do Cartão SUS</th>
                                        <th>Telefone</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach ($pacientes as $paciente) : ?>

                                        <tr>
                                            <td><?php echo $paciente['nome'] ?></td>
                                            <td><?php echo $paciente['sexo'] ?></td>
                                            <td><?php echo $paciente['num_sus'] ?></td>
                                            <td><?php echo $paciente['contato'] ?></td>
                                            <td>
                                                <form action="formEdicao.php" method="POST">
                                                    <button type="button" class="btn btn-success btn-xs">Histórico</button>
                                                    <button type="button" class="btn btn-xs btn-info" id="id" data-toggle="modal" data-target="#myModal1" data-id="<?php echo $paciente['id_paciente'] ?>">Mais Informações</button>

                                                    <input type="hidden" name="idPaciente" value="<?php echo $paciente['id_paciente']?>">
                                                    <button type="submit" class="btn btn-xs btn-danger">Editar</button>
                                                </form>

                                            </td>                                            
                                        </tr>


                                    <?php endforeach ?>

                            </tbody>
                            </table>
                        </div>
                    </div>

            </div>
            <?php require_once "modalInformacoesPaciente.php" ?>
        </div>

    </div>

    <script src="/public/plugins/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="/public/plugins/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="modalDetalhes.js"></script>
<?php
$template->fimbody();
?>
