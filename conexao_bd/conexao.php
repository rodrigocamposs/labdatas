<?php

    define('DB_DRIVE', 'mysql');
    define('DB_HOSTNAME', 'www.db4free.net');
    define('DB_DATABASE', 'labdatas');
    define('DB_USERNAME', 'labdatas');
    define('DB_PASSWORD', 'labdatas123');
    define('DB_CHARSET', 'utf8');

    class Conexao  extends PDO {

        private $dbh;
        private $stmt;        

        function __construct() {

            # Array de parâmetros da conexão
            $options = array (
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
            );

            try {
                # Chamo o construtor da classe que foi extendida PDO. Os parametros estão vindo das constantes definidas no Config.php
                parent::__construct(DB_DRIVE . ':host=' . DB_HOSTNAME . ';dbname=' . DB_DATABASE .';charset='.DB_CHARSET, DB_USERNAME, DB_PASSWORD, $options);
            } catch (\PDOException $e) {

                # Caso alguma coisa dê errado.
                exit('Não foi possível estabelecer conexão com o banco de dados: ' . $e->getMessage());
            }

        }

        public function closeConnection() {

            try {
                unset($this->dbh);
            } catch (PDOException $e) {
                die ('Erro ao tentar fechar a conexao com o banco de dados: ' . $e->getMessage());
            }
                
        }


}