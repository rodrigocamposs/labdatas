<?php

require_once "../conexao_bd/conexao.php";
include "../lib/Datetime.php";
include "../views/template/layout.php";

class Agenda {

    private $conexao;

    const PADRAO_TEMPO_EXAME = '50 min';

    public function __construct() {
        $this->conexao = new Conexao;
    }

    public function insert($dados) {
        $dados['data_hora_inicial'] = LabDatas_Datetime::format($dados['data_hora_inicial'], 'Y-m-d H:i:s');
        if (empty($dados['data_hora_final'])) {
            $data_hora_final = implode(' ', [$dados['data_hora_inicial'], self::PADRAO_TEMPO_EXAME]);
            $dt = new DateTime($data_hora_final);
            $dados['data_hora_final'] = $dt->format('Y-m-d H:i:s');
        } else {
            $dados['data_hora_final'] = LabDatas_Datetime::format($dados['data_hora_final'], 'Y-m-d H:i:s');
        }
        try {

            $sql = 'INSERT INTO exames_agenda ';
            $sql .= '(id_paciente,id_tipoexame,id_medico,data_hora_inicial,data_hora_final,id_local) ';
            $sql .= 'VALUES (:id_paciente,:id_tipoexame,:id_medico,:data_hora_inicial,:data_hora_final,:id_local)';

            $stmt = $this->conexao->prepare($sql);

            $stmt->bindParam(':id_paciente', $dados['id_paciente']);
            $stmt->bindParam(':id_tipoexame', $dados['id_tipoexame']);
            $stmt->bindParam(':id_medico', $dados['id_medico']);
            $stmt->bindParam(':data_hora_inicial', $dados['data_hora_inicial']);
            $stmt->bindParam(':data_hora_final', $dados['data_hora_final']);
            $stmt->bindParam(':id_local', $dados['id_local']);

            $stmt->execute();

            return $this->conexao->lastInsertId();
        } catch (Exception $e) {
            echo "Erro ao inserir: " . $e->getMessage();
        }
    }

    public function buscaAgendamento($id_agenda) {

        $sql = ' SELECT * FROM exames_agenda ';
        $sql .= ' WHERE id_agenda = :id_agenda ';

        $stmt = $this->conexao->prepare($sql);

        $stmt->bindParam(':id_agenda', $id_agenda, PDO::PARAM_INT);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row;
    }

    public function editaAgendamento(array $data) {
        $data['data_hora_inicial'] = LabDatas_Datetime::format($data['data_hora_inicial'], 'Y-m-d H:i:s');
        $data['data_hora_final'] = LabDatas_Datetime::format($data['data_hora_final'], 'Y-m-d H:i:s');

        $sql = ' UPDATE exames_agenda ';
        $sql .= ' SET id_paciente = :id_paciente, id_tipoexame = :id_tipoexame, ';
        $sql .= ' id_medico = :id_medico, data_hora_inicial = :data_hora_inicial, ';
        $sql .= ' data_hora_final = :data_hora_final, id_local = :id_local ';
        $sql .= ' WHERE id_agenda = :id_agenda ';

        $stmt = $this->conexao->prepare($sql);

        $stmt->bindParam(':id_agenda', $data['id_agenda']);
        $stmt->bindParam(':id_paciente', $data['id_paciente']);
        $stmt->bindParam(':id_tipoexame', $data['id_tipoexame']);
        $stmt->bindParam(':id_medico', $data['id_medico']);
        $stmt->bindParam(':data_hora_inicial', $data['data_hora_inicial']);
        $stmt->bindParam(':data_hora_final', $data['data_hora_final']);
        $stmt->bindParam(':id_local', $data['id_local']);

        return $stmt->execute();
    }

    public function atualizaAgendamento(array $data) {
        $data['data_hora_inicial'] = LabDatas_Datetime::format($data['data_hora_inicial'], 'Y-m-d H:i:s');
        $data['data_hora_final'] = LabDatas_Datetime::format($data['data_hora_final'], 'Y-m-d H:i:s');

        $sql = ' UPDATE exames_agenda ';
        $sql .= ' SET data_hora_inicial = :data_hora_inicial, data_hora_final = :data_hora_final ';
        $sql .= ' WHERE id_agenda = :id_agenda ';

        $stmt = $this->conexao->prepare($sql);

        $stmt->bindParam(':id_agenda', $data['id_agenda']);
        $stmt->bindParam(':data_hora_inicial', $data['data_hora_inicial']);
        $stmt->bindParam(':data_hora_final', $data['data_hora_final']);

        return $stmt->execute();
    }

    public function excluiAgendamento(array $data) {
        $sql = ' DELETE FROM exames_agenda ';
        $sql .= ' WHERE id_agenda = :id_agenda ';

        $stmt = $this->conexao->prepare($sql);

        $stmt->bindParam(':id_agenda', $data['id_agenda']);

        return $stmt->execute();
    }

    public function pesquisaAgendamentos($pesquisa) {
        $sql = ' SELECT GROUP_CONCAT(DISTINCT a.id_agenda SEPARATOR ",") as id_agenda, ';
        $sql .= ' p.nome, MIN(data_hora_inicial) as start, MAX(data_hora_final) as end ';
        $sql .= ' FROM exames_agenda a ';
        $sql .= ' INNER JOIN pacientes p USING (id_paciente) ';
        $sql .= ' INNER JOIN medicos m USING (id_medico) ';
        $sql .= ' INNER JOIN exames_tipoexame te USING (id_tipoexame) ';
        $sql .= ' WHERE p.nome LIKE :termo OR ';
        $sql .= ' m.no_medico LIKE :termo OR ';
        $sql .= ' te.no_exame LIKE :termo ';

        $stmt = $this->conexao->prepare($sql);
        $pesquisa = '%' . $pesquisa . '%';
        $stmt->bindParam(':termo', $pesquisa);

        $stmt->execute();
        $rowset = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($rowset as $row) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($row->end)));
            $return['id_agenda'] = $row->id_agenda;
            $return['start'] = $row->start;
            $return['end'] = $end;
        }
        return $return;
    }

    public function visualizaAgendamentos(array $data) {
        $start = (new DateTime($data['start']))->format('Y-m-d H:M:i');
        $end = (new DateTime($data['end']))->format('Y-m-d H:M:i');
        $sql = ' SELECT * FROM exames_agenda ';
        $sql .= ' INNER JOIN pacientes USING (id_paciente) ';
        $sql .= ' INNER JOIN medicos USING (id_medico) ';
        $sql .= ' INNER JOIN exames_tipoexame USING (id_tipoexame) ';
        $sql .= ' WHERE data_hora_inicial BETWEEN :start AND :end OR ';
        $sql .= ' data_hora_final BETWEEN :start AND :end OR ';
        $sql .= ' data_hora_inicial <= :start AND data_hora_final >= :end ';

        $stmt = $this->conexao->prepare($sql);
        $stmt->bindParam(':start', $start);
        $stmt->bindParam(':end', $end);
        $stmt->execute();
        $rowset = $stmt->fetchAll(PDO::FETCH_OBJ);

        $events = [];
        $colors = (new Layout())->colors();
        foreach ($rowset as $row) {
            $color = current($colors);
            if (!next($colors)) {
                reset($colors);
            }
            $medico = implode(' ', [$row->mm_tratamento, $row->no_medico]);
            $description = '<ul>';
            $description .= '<li>Médico: ' . $medico . '</li>';
            $description .= '<li>Exame: ' . $row->no_exame . '</li>';
            $description .= '</ul>';
            $events[] = [
                'id' => $row->id_agenda,
                'title' => $row->nome,
                'description' => $description,
                'start' => $row->data_hora_inicial,
                'end' => $row->data_hora_final,
                'color' => $color
            ];
        }

        return $events;
    }

}
