<?php

class Medicos {

    static function getMedicos() {
        $medicos = [];
        $sql = ' SELECT * FROM medicos ';
        $sql .= ' ORDER by no_medico ';
        $conn = new Conexao();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $rowset = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($rowset as $row) {
            $medicos[$row->id_medico] = implode(' ', [$row->mm_tratamento, strtoupper($row->no_medico)]);
        }
        return $medicos;
    }

}
