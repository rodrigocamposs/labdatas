<?php

class Local {

    static function getLocais() {
        $locais = [];
        $sql = ' SELECT * FROM exames_local ';
        $sql .= ' ORDER by no_local ';
        $conn = new Conexao();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $rowset = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($rowset as $row) {
            $locais[$row->id_local] = strtoupper($row->no_local);
        }
        return $locais;
    }

}
