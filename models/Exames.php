<?php

class Exames {

    static function getTipoExames() {
        $exames = [];
        $sql = ' SELECT * FROM exames_tipoexame ';
        $sql .= ' ORDER by no_exame ';
        $conn = new Conexao();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $rowset = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($rowset as $row) {
            $exames[$row->id_tipoexame] = strtoupper($row->no_exame);
        }
        return $exames;
    }

}
