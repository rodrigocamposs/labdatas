<?php

    require_once "../conexao_bd/conexao.php";
    class Pacientes
    {

    private $conexao;

    public function __construct() {
      $this->conexao = new Conexao;
    }        

        public function insert($dados)
        {
            try {

                $sql = "INSERT INTO pacientes (nome, sexo, nome_mae, rg, data_nasc, num_sus, contato,endereco) VALUES (:nome_paciente, :sexo_paciente, :nome_mae, :rg_paciente, :data_nasc, :num_sus, :telefone, :endereco)";

                $stmt = $this->conexao->prepare($sql); 

                $stmt->bindParam(':nome_paciente', $dados['nome_paciente']);
                $stmt->bindParam(':sexo_paciente', $dados['sexo']);
                $stmt->bindParam(':nome_mae', $dados['nome_mae']);
                $stmt->bindParam(':rg_paciente', $dados['rg_paciente']);
                $stmt->bindParam(':data_nasc', $dados['data_nasc']);
                $stmt->bindParam(':num_sus', $dados['num_sus']);
                $stmt->bindParam(':telefone', $dados['telefone']);
                $stmt->bindParam(':endereco', $dados['endereco']);

                $stmt->execute();

            } catch (Exception $e) {
                echo "Erro ao inserir: " . $e->getMessage();
            }

        }

        public function buscaPaciente($id_paciente)
        {

          $sql = "SELECT * FROM pacientes WHERE id_paciente = :id_paciente ";

          $stmt = $this->conexao->prepare($sql);

          $stmt->bindParam(':id_paciente', $id_paciente);

          $stmt->execute();

          $row = $stmt->fetch(PDO::FETCH_ASSOC);

          return $row;
          
        }     

        public function editaPaciente($dados)
        {

          $sql = "UPDATE pacientes SET nome = :nome, sexo = :sexo, nome_mae = :nome_mae, rg = :rg_paciente, data_nasc = :data_nasc, num_sus = :num_sus, contato = :contato, endereco = :endereco WHERE id_paciente = :id_paciente ";

          $stmt = $this->conexao->prepare($sql);

          $stmt->bindParam(':id_paciente', $dados['id_paciente']);
          $stmt->bindParam(':nome', $dados['nome']);
          $stmt->bindParam(':sexo', $dados['sexo']);
          $stmt->bindParam(':nome_mae', $dados['nome_mae']);
          $stmt->bindParam(':rg_paciente', $dados['rg_paciente']);
          $stmt->bindParam(':data_nasc', $dados['data_nasc']);
          $stmt->bindParam(':num_sus', $dados['num_sus']);
          $stmt->bindParam(':contato', $dados['contato']);
          $stmt->bindParam(':endereco', $dados['endereco']);

          $stmt->execute();
          
        }             


        public function visualizaPacientes() {

            $sql = "SELECT * FROM pacientes";

            $stmt = $this->conexao->prepare($sql);

            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $rows;

        }

        public function insereFundos($dados)
        {
            try {

                $sql = "INSERT INTO pacientes (nome, sexo, nome_mae, rg, data_nasc, num_sus, contato,endereco) VALUES (:nome_paciente, :sexo_paciente, :nome_mae, :rg_paciente, :data_nasc, :num_sus, :telefone, :endereco)";

                $stmt = $this->conexao->prepare($sql); 

                $stmt->bindParam(':nome_paciente', $dados['nome_paciente']);
                $stmt->bindParam(':sexo_paciente', $dados['sexo']);
                $stmt->bindParam(':nome_mae', $dados['nome_mae']);
                $stmt->bindParam(':rg_paciente', $dados['rg_paciente']);
                $stmt->bindParam(':data_nasc', $dados['data_nasc']);
                $stmt->bindParam(':num_sus', $dados['num_sus']);
                $stmt->bindParam(':telefone', $dados['telefone']);
                $stmt->bindParam(':endereco', $dados['endereco']);

                $stmt->execute();

            } catch (Exception $e) {
                echo "Erro ao inserir: " . $e->getMessage();
            }

        }
        
        static function getPacientes() {
            $sql = ' SELECT * FROM pacientes ';
            $sql .= ' ORDER by nome ';
            $conn = new Conexao();
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $rowset = $stmt->fetchAll(PDO::FETCH_OBJ);
            $pacientes = [];
            foreach ($rowset as $row) {
                $pacientes[$row->id_paciente] = strtoupper($row->nome);
            }
            return $pacientes;
        }

    }
