<?php

class LabDatas_Datetime {

    /**
     * Formatar data para mostrá-la no formato:
     * 'd/m/Y' => eGI_DateTime::DATE_MEDIUM
     * 'Y-m-d\TH:i:sO' => eGI_DateTime::DATE_MYSQL
     * @param string $date  'd/m/Y', 'Y-m-d'
     * @param string $format
     * @return string Data no formato definido
     */
    static function format($date, $format = 'Y-m-d') {
        $flag = false;
        $formatos = [
            'Y-m-d',
            'd/m/y',
            'd/m/Y',
            'd/m/Y H:i',
            'd/m/Y H:i:s',
            'Y-m-d H:i',
            'Y-m-d H:i:s'
        ];
        foreach ($formatos as $formato) {
            $dt = DateTime::createFromFormat($formato, $date);
            if ($dt) {
                $flag = true;
                $date = $dt->format($format);
                break;
            }
        }
        return $flag ? $date : null;
    }

}
